export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
       _name: 'CSidebarNavTitle',
       _children: ['Menu']
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Territorios',
        route: '/base',
        icon: 'cil-map',
        items: [
          {
            name: 'Designar',
            to: '/base/breadcrumbs'
          },
          {
            name: 'Buscar',
            to: '/base/cards'
          }
        ]
      },
      {
        _name: 'CSidebarNavItem',
        name: 'Publicadores',
        to: '/publishers',
        icon: 'cil-user'
      },
      {
        _name: 'CSidebarNavDropdown',
        name: 'Relatórios',
        route: '/campofone',
        icon: 'cil-spreadsheet',
        items: [
          {
            name: 'Faixas de Território',
            to: '/reports'
          },
          {
            name: 'Participação',
            to: '/reports'
          }
        ]
      },
      {
        _name: 'CSidebarNavItem',
        name: 'Ajustes',
        to: '/charts',
        icon: 'cil-settings'
      },
    ]
  }    
]