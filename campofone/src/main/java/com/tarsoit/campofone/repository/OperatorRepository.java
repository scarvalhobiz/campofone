package com.tarsoit.campofone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tarsoit.campofone.entity.Operator;

@Repository
public interface OperatorRepository extends JpaRepository<Operator, Long> { }
