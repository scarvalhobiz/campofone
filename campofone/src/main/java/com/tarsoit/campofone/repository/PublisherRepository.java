package com.tarsoit.campofone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tarsoit.campofone.entity.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> { }
