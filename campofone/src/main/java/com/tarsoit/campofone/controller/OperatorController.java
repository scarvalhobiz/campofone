package com.tarsoit.campofone.controller;


import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tarsoit.campofone.entity.Operator;
import com.tarsoit.campofone.repository.OperatorRepository;

@RestController
public class OperatorController {
    @Autowired
    private OperatorRepository _operatorRepository;

    @RequestMapping(value = "/operator", method = RequestMethod.GET)
    public List<Operator> Get() {
        return _operatorRepository.findAll();
    }

    @RequestMapping(value = "/operator/{id}", method = RequestMethod.GET)
    public ResponseEntity<Operator> GetById(@PathVariable(value = "id") long id)
    {
        Optional<Operator> operator = _operatorRepository.findById(id);
        if(operator.isPresent())
            return new ResponseEntity<Operator>(operator.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/operator", method =  RequestMethod.POST)
    public Operator Post(@Valid @RequestBody Operator operator)
    {
        return _operatorRepository.save(operator);
    }

    @RequestMapping(value = "/operator/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Operator> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Operator newOperator)
    {
        Optional<Operator> oldOperator = _operatorRepository.findById(id);
        if(oldOperator.isPresent()){
            Operator operator = oldOperator.get();
            operator.setNome(newOperator.getNome());
            _operatorRepository.save(operator);
            return new ResponseEntity<Operator>(operator, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/operator/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
    {
        Optional<Operator> operator = _operatorRepository.findById(id);
        if(operator.isPresent()){
            _operatorRepository.delete(operator.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}