package com.tarsoit.campofone.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tarsoit.campofone.entity.Publisher;
import com.tarsoit.campofone.repository.PublisherRepository;

@RestController
public class PublisherController {
    @Autowired
    private PublisherRepository _publisherRepository;

    @RequestMapping(value = "/publisher", method = RequestMethod.GET)
    public List<Publisher> Get() {
        return _publisherRepository.findAll();
    }

    @RequestMapping(value = "/publisher/{id}", method = RequestMethod.GET)
    public ResponseEntity<Publisher> GetById(@PathVariable(value = "id") long id)
    {
        Optional<Publisher> publisher = _publisherRepository.findById(id);
        if(publisher.isPresent())
            return new ResponseEntity<Publisher>(publisher.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/publisher", method =  RequestMethod.POST)
    public Publisher Post(@Valid @RequestBody Publisher publisher)
    {
        return _publisherRepository.save(publisher);
    }

    @RequestMapping(value = "/publisher/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<Publisher> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Publisher newPublisher)
    {
        Optional<Publisher> oldPublisher = _publisherRepository.findById(id);
        if(oldPublisher.isPresent()){
            Publisher publisher = oldPublisher.get();
            publisher.setName(newPublisher.getName());
            _publisherRepository.save(publisher);
            return new ResponseEntity<Publisher>(publisher, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/publisher/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id)
    {
        Optional<Publisher> publisher = _publisherRepository.findById(id);
        if(publisher.isPresent()){
            _publisherRepository.delete(publisher.get());
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}